<?php
/**
 * @file
 * Token i18n module basic tests.
 */

/**
 * I have used the t() function according to SimpleTest rules
 * https://drupal.org/simpletest-tutorial-drupal7
 *
 * Main points:
 * 1. "Do not use t() in: Strings in getInfo()."
 * 2. "[...]for the test runner's information only, and so it is not translated"
 */

class Tokeni18nMacronBasicTestsCase extends DrupalWebTestCase {
  /**
   * Implements getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Token i18n Macron Module',
      'description' => 'Checks the module and all currently enabled hooks.',
      'group' => 'Tokeni18nMacron',
    );
  }

  /**
   * Prepares database for the tests.
   */
  public function setUp() {
    // Enable any modules required for the test.
    parent::setUp('token_i18n_macrons');
  }

  /**
   * This is the base of all tests, since multiple databases are not needed.
   */
  public function testBaseTest() {
    $this->subTestBasicStrings();
    $this->subTestEmptyStrings();
  }

  /**
   * Tests a predefined set of strings who have no obvious ASCII equivilant.
   */
  protected function subTestBasicStrings() {
    // Basic tests, given input and expected output.
    foreach ($this->tokensForBasicStrings() as $input => $expected) {
      // Run the function and find the output.
      $output = token_i18n_macrons_flatten($input);
      // Prepare an error message.
      $message = format_string(
        "Properly flattened string below:\nInput:\n - !input\nOutput\n - '!output' (!output_len)\nExpected:\n - '!expected' (!expected_len)",
        array(
          '!input' => $input,
          '!expected' => $expected,
          '!output' => $output,
          '!expected_len' => drupal_strlen($expected),
          '!output_len' => drupal_strlen($output),
        )
      );
      // Run assertion.
      $this->assertIdentical($expected, $output, $message);
    }
  }

  /**
   * Tests majority of unicode characters.
   *
   * This ensures that if a character is not specified then it will be removed.
   */
  protected function subTestEmptyStrings() {
    // Get a list of characters to avoid.
    $exclude = array(); $options = array();
    drupal_alter('token_i18n_macrons_dictionary_options', $exclude, $options);
    $exclude = array_keys($exclude);

    // Postgres does not like many unicode characters above 55296.
    // Check the first 55,295 unicode characters, see if they aren't removed.
    // 1-256 are ignored as they are extended ASCII, which we want.
    for ($i = 256; $i < 55296; $i++) {
      // Prepare a unicode character to test.
      $string = mb_convert_encoding(sprintf('&#%d;', $i), 'UTF-8', 'HTML-ENTITIES');

      // Ensure it is not already zero length.
      $string_len = drupal_strlen($string);
      if ($string_len === 0) {
        // Can't use this character to test.
        continue;
      }

      // If the character has been translated.
      if (in_array($string, $exclude)) {
        // Then skip it.
        continue;
      }

      // Run the tested function over it.
      $output = token_i18n_macrons_flatten($string);

      // Testing if length is zero instead of empty string
      // because Unicode is crazy. Now you know.
      $output_len = drupal_strlen($output);
      // Prepare an error message.
      $message = format_string(
        "Properly flattened string below:\nInput:\n - !input\nOutput\n - '!output' (!output_len)",
        array(
          '!input' => $string,
          '!output' => $output,
          '!output_len' => drupal_strlen($output),
        )
      );
      // Run assertion.
      $this->assertIdentical($output_len, 0, $message);
    }
  }

  /**
   * A simple list of string tokens for Basic String test.
   *
   * @return array
   *   Array(string to flatten => flattened string)
   */
  protected function tokensForBasicStrings() {
    // These tests were chosen as they have no obvious ASCII equivilant
    // so are unlikely to interfere with other module's hooks.
    return array(
      'abcabæc¥aฏbcd¿'                                    => 'abcabcabcd',
      'وچ، مجموعه مرجع فله ددهه مپوتر  تعدد مورد مربوط م' => '          ',
      'किसी बाधा के विभिन्न प्रणालियों से'                     => '     ',
      'ಇವೇ ಮೊದಲಾದ ಆಪರೇಟಿಂಗ್ ಸಿಸ್ಟಮ್'                           => '   ',
      'यिम ज़न दुनिय॒हक्यन वार्यहन मुल्कन'                       => '    ',
      '하e여여름x日光a節約m時p間l制e'                     => 'example',
    );
  }
}
